# Smart Home 2

## Opraveno
| Výtka                  | Výsledek                                              |
| ---------------------- | ----------------------------------------------------- |
| vytvořit složku pro diagramy            | hotovo;                              |
| ohraničení systému v UC diagramech      | hotovo;                              |
| používat polymorfismus                  | hotovo;                              |
| pročistit static věci                   | napůl - co šlo, přeměněno ze "static" na "member" (non-static), nebo přesunuto do tříd Constants a ApplicationState;                                                                |
| naformátovat si kód                     | napůl - dva prázdné řádky používáme k oddělení "oddílů", oddíly obsahují buď související metody, nebo související atributy;                                                            |
| dodělat DeviceAPI                       | chybí - nedoděláno                   |
| složkám počáteční písmena přepsat na malá    | hotovo                          |
| přesunout třídu s metodou main do kořenové složky, připsat javadoc | hotovo    |
| přidat výjimky                               | napůl - přidali jsme jednu;     |
| explicitní hodnoty ukládat do proměnných viz random čísla apod. | hotovo       |
| testy nejsou                                 | chybí - stále nejsou            |

## Design patterny

Zkonzultováno
* Iterator - iterace spotřebiči a stroji v domě při každém simulation loopu (od horního patra do spodního, pokoje v patře např. abecedně/dle parametru, spotřebiče v pokoji obdobně)
* Visitor/Observer/Listener - člověk čekající na dokončení činnosti zařízení
* Singleton - manuál ke každému spotřebiči
* Lazy Initialization - návody a manuály k zařízením
* Builder - vytvoření objektů pomoсí knihovny Jackson
* Template

## Funkční požadavky
* F1.	Entity se kterými pracujeme je dům, okno (+ venkovní žaluzie), patro v domu, senzor, zařízení (=spotřebič), osoba, auto, kolo, domácí zvíře jiného než hospodářského typu, plus libovolné další entity.
* F2.	Jednotlivá zařízení v domu mají API na ovládání. Zařízení mají stav, který lze měnit pomocí API na jeho ovládání. Akce z API jsou použitelné podle stavu zařízení. Vybraná zařízení mohou mít i obsah - lednice má jídlo, CD přehrávač má CD.
* F3.	Spotřebiče mají svojí spotřebu v aktivním stavu, idle stavu, vypnutém stavu.
* F4.	Jednotlivá zařízení mají API na sběr dat o tomto zařízení. O zařízeních sbíráme data jako spotřeba elektřiny, plynu, vody a funkčnost.
* F5.	Jednotlivé osoby a zvířata mohou provádět aktivity(akce), které mají nějaký efekt na zařízení nebo jinou osobu.
* F6.	Jednotlivá zařízení a osoby se v každém okamžiku vyskytují v jedné místnosti (pokud nesportují) a náhodně generují eventy.
* F7.	Eventy jsou přebírány a odbavovány vhodnou osobou (osobami) nebo zařízením (zařízeními).
* F8.	Vygenerování reportů.
* F9.	Při rozbití zařízení musí obyvatel domu prozkoumat dokumentaci k zařízení - najít záruční list, projít manuál na opravu a provést nápravnou akcí.
* F10.	Rodina je aktivní a volný čas tráví zhruba v poměru 50% používání spotřebičů v domě a 50% sport. Když není volné zařízení nebo sportovní náčiní, tak osoba čeká.

| Funkční požadavek      | Obsahuje třída |
| ---------------------- | ----------- |
| F1      | Home, Window, Floor, Sensor, Device, Person, Pet, Car, Bicycle,...        |
| F2   | Device a její podtřídy        |
| F3   | PowerState        |
| F4   | Consumption        |
| F5   | Event - metoda affectParticipants()         |
| F6   | Eventor - metoda act()        |
| F7   | Výběr odbavovatele podle atributu PURPOSE, implementováno v metodě act() Eventorů        |
| F8   | Reporter        |
| F9   | Repair, Manual        |
| F10   | Eventor - metoda act()        |


## Class diagram
![](/diagrams/CD_SmartHome2-Class%20diagram.drawio.png)

## Use case diagram
![](/diagrams/UC%20diagram.png)
