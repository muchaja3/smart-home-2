package events;

import actors.Eventor;
import actors.houseMembers.HouseMember;
import general.Logger;

import java.util.List;

public class Waiting extends Event {

    public Waiting(int durationInSecs, Eventor originator) {
        super(durationInSecs, originator);
        Logger.getInstance().log("A Waiting happened.");
    }


    @Override
    protected void initParticipants(Eventor... otherParticipants) {
        //no action - no participants other than the originator
    }

    @Override
    public void affectParticipants() {
        getOriginator().becomeBusyForSecs(getDurationInSecs());
        ((HouseMember) getOriginator()).changeEnergy(getDurationInSecs());
    }

    @Override
    public List<Eventor> getParticipants() {
        return List.of(getOriginator());
    }
}
