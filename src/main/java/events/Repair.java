package events;

import actors.devices.Device;
import actors.devices.Manual;
import actors.Eventor;
import general.Logger;

import java.util.List;
public class Repair extends Event {

    private Device repairedTarget;

    public Repair(int durationInSecs, Eventor originator, Device repairedTarget) {
        super(durationInSecs, originator, repairedTarget);
        Logger.getInstance().log("A Repair happened.");
    }

    @Override
    protected void initParticipants(Eventor... otherParticipants) {
        this.repairedTarget = (Device) otherParticipants[0];
    }

    @Override
    public void affectParticipants() {
        this.getOriginator().becomeBusyForSecs(this.getDurationInSecs());
        repairedTarget.becomeBusyForSecs(this.getDurationInSecs());

        Manual.buildManualText(repairedTarget);
        repairedTarget.repair();
    }

    @Override
    public List<Eventor> getParticipants() {
        return List.of(getOriginator(), repairedTarget);
    }

    public Device getRepairedTarget() {
        return repairedTarget;
    }
}
