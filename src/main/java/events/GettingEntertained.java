package events;

import actors.Eventor;
import actors.houseMembers.HouseMember;
import general.Logger;

import java.util.List;

public class GettingEntertained extends Event {

    private Eventor entertainer;

    public GettingEntertained(int durationInSecs, Eventor entertained, Eventor entertainer) {
        super(durationInSecs, entertained, entertainer);
        Logger.getInstance().log("A GettingEntertained happened.");
    }

    @Override
    protected void initParticipants(Eventor... otherParticipants) {
        this.entertainer = otherParticipants[0];
    }

    @Override
    public void affectParticipants() {
        Logger.getInstance().log("Affecting participants of " + this);
        getOriginator().becomeBusyForSecs(this.getDurationInSecs());
        entertainer.becomeBusyForSecs(this.getDurationInSecs());

        ((HouseMember) getOriginator()).changeAmusement(
                entertainer.getFunWithinDefinedTime(this.getDurationInSecs()));
    }

    @Override
    public List<Eventor> getParticipants() {
        return List.of(getOriginator(), entertainer);
    }

    public Eventor getEntertainer() {
        return entertainer;
    }


    @Override
    public String toString() {
        return "GettingEntertained{" +
                "durationInSecs=" + getDurationInSecs() +
                ", originator=" + getOriginator() +
                ", entertainer=" + entertainer +
                '}';
    }

}
