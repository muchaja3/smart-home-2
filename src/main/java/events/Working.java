package events;

import actors.devices.Device;
import actors.Eventor;
import general.Logger;

import java.util.List;

public class Working extends Event {

    private Device usedDevice;


    public Working(int durationInSecs, Eventor originator, Device usedDevice) {
        super(durationInSecs, originator, usedDevice);
        Logger.getInstance().log("A Working happened.");
    }


    @Override
    protected void initParticipants(Eventor... otherParticipants) {
        this.usedDevice = (Device) otherParticipants[0];
    }

    @Override
    public void affectParticipants() {
        this.getOriginator().becomeBusyForSecs(this.getDurationInSecs());
        usedDevice.switchOn();
        usedDevice.becomeBusyForSecs(this.getDurationInSecs());
    }

    @Override
    public List<Eventor> getParticipants() {
        return List.of(getOriginator(), usedDevice);
    }

    public Device getUsedDevice() {
        return usedDevice;
    }
}