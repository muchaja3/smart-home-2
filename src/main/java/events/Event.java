package events;


import actors.Eventor;
import general.Reporter;

import java.util.List;

public abstract class Event {

    private final int durationInSecs;
    private final Eventor originator;


    protected Event(int durationInSecs, Eventor originator, Eventor... otherParticipants) {
        this.durationInSecs = durationInSecs;
        this.originator = originator;
        initParticipants(otherParticipants);
        Reporter.getInstance().recordEvent(this);
        affectParticipants();
    }

    /**
     * Initialize all participants of the event other than the originator.
     *
     * @param otherParticipants Eventors from which the participants are picked.
     */
    protected abstract void initParticipants(Eventor... otherParticipants);

    /**
     * Express effects of this event on all of it's participants.
     */
    protected abstract void affectParticipants();

    /**
     * Return all of it's participants - originator included.
     *
     * @return List of Eventors that participate in this event.
     */
    public abstract List<Eventor> getParticipants();

    public int getDurationInSecs() {
        return durationInSecs;
    }

    public Eventor getOriginator() {
        return originator;
    }
}
