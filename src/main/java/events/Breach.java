package events;

import actors.devices.Device;
import actors.Eventor;
import general.Logger;

import java.util.List;

public class Breach extends Event {

    private Device brokenTarget;


    public Breach(Device brokenDevice) {
        super(0, brokenDevice);
        Logger.getInstance().log("A Breach happened.");
    }


    @Override
    protected void initParticipants(Eventor... otherParticipants) {
        this.brokenTarget = (Device) otherParticipants[0];
    }

    @Override
    public void affectParticipants() {
        brokenTarget.doBreak();
    }

    @Override
    public List<Eventor> getParticipants() {
        return List.of(getOriginator(), brokenTarget);
    }

    public Device getBrokenTarget() {
        return brokenTarget;
    }
}
