package events;

import actors.Eventor;
import general.Logger;

import java.util.List;

public class Sporting extends Event {

    private Eventor tool;

    public Sporting(int durationInSecs, Eventor originator, Eventor tool) {
        super(durationInSecs, originator, tool);
        Logger.getInstance().log("A Sporting happened.");
    }


    @Override
    protected void initParticipants(Eventor... otherParticipants) {
        this.tool = otherParticipants[0];
    }

    @Override
    public void affectParticipants() {
        this.getOriginator().becomeBusyForSecs(this.getDurationInSecs());
        tool.becomeBusyForSecs(this.getDurationInSecs());
    }

    @Override
    public List<Eventor> getParticipants() {
        return List.of(getOriginator(), tool);
    }

    public Eventor getTool() {
        return tool;
    }

    @Override
    public String toString() {
        return "Sporting{" +
                "originator=" + getOriginator() +
                "tool=" + tool +
                '}';
    }
}
