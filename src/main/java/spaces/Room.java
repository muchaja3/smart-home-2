package spaces;

import actors.Eventor;
import actors.iterating.Quantifyable;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.beans.ConstructorProperties;
import java.util.ArrayList;
import java.util.List;

public class Room implements Quantifyable<Eventor> {

    private final String name;

    private final Floor floor;

    @JsonManagedReference
    private final List<Eventor> eventors;

    @ConstructorProperties({"name", "floor", "eventors"})
    public Room(String name, Floor floor, List<Eventor> eventors) {
        this.name = name;
        this.floor = floor;
        this.eventors = eventors;
    }

    @Override
    public boolean hasNoEventors() {
        return eventors.isEmpty();
    }

    @Override
    public List<Eventor> getQuantifiedCollection() {
        return eventors;
    }

    public String getName() {
        return name;
    }

    public Floor getFloor() {
        return floor;
    }

    public List<Eventor> getEventors() {
        return eventors;
    }

    @Override
    public String toString() {
        return "Room{" +
                "name='" + name + '\'' +
                ", floor=" + floor +
                '}';
    }
}
