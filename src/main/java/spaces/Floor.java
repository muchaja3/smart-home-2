package spaces;

import actors.iterating.Quantifyable;

import java.beans.ConstructorProperties;
import java.util.ArrayList;
import java.util.List;

public class Floor implements Quantifyable<Room> {

    private final String name;

    private final int number;

    private final House house;

    private final List<Room> rooms;

    @ConstructorProperties({"name", "number", "house", "rooms"})
    public Floor(String name, int number, House house, List<Room> rooms) {
        this.name = name;
        this.number = number;
        this.house = house;
        this.rooms = rooms;
    }

    @Override
    public List<Room> getQuantifiedCollection() {
        return rooms;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public House getHouse() {
        return house;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    @Override
    public String toString() {
        return "Floor{" +
                "name='" + name + '\'' +
                ", number=" + number +
                ", rooms=" + rooms +
                '}';
    }
}
