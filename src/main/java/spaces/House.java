package spaces;

import actors.iterating.EventorsCollection;
import actors.iterating.Quantifyable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.beans.ConstructorProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class House implements Quantifyable<Floor> {

    private final String familyName;

    private final List<Floor> floors;

    private final EventorsCollection eventorsCollection = new EventorsCollection(this);


    @ConstructorProperties({"familyName", "floors"})
    public House(String familyName, List<Floor> floors) {
        this.familyName = familyName;
        this.floors = floors;
    }

    @Override
    public List<Floor> getQuantifiedCollection() {
        return floors;
    }

    public String getFamilyName() {
        return familyName;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    /**
     * Return all eventors in this configuration run.
     * @return {@code EventorsCollection} containing all {@code Eventors} in this configuration run.
     */
    public EventorsCollection getEventorsCollection() {
        return eventorsCollection;
    }
}
