package actors;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Purpose {
    HUMAN_ENTERTAINMENT, PET_ENTERTAINMENT, SPORT, WORK

}
