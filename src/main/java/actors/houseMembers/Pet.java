package actors.houseMembers;

import actors.Eventor;
import actors.Purpose;
import general.ApplicationState;
import general.Constants;
import general.Logger;
import events.GettingEntertained;
import events.Waiting;
import spaces.Room;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.beans.ConstructorProperties;

@JsonTypeName("pet")
public class Pet extends HouseMember {

    private final int playfulness;

    @ConstructorProperties({"name", "room", "familyStatus", "age", "isMaleNotFemale","energy", "maxEnergy", "amusement", "playfulness", "funPerSec"})
    public Pet(String name, Room room, FamilyStatus familyStatus, int age, boolean isMaleNotFemale, int energy, int maxEnergy, int amusement, int playfulness, int funPerSec) {
        super(name, room, familyStatus, age, isMaleNotFemale, energy, maxEnergy, amusement);
        this.playfulness = playfulness;
        this.funPerSec = funPerSec;
        Logger.getInstance().log(Logger.Level.INFO, "created a dog " + this);
    }

    @Override
    public int getFunWithinDefinedTime(int timeInSecs) {
        if (getEnergy() > timeInSecs) {
            return timeInSecs * getFunPerSec();
        } else {
            int funnyPart = getEnergy() * getFunPerSec();
            return funnyPart + (timeInSecs - getEnergy()) / 2;       //when they don't want to play anymore, there is not that fun in playing with them
        }
    }


    @Override
    protected void actToLive(int periodInSecs) {
        Logger.getInstance().log(Logger.Level.DETAIL, this + " is deciding what to do with his turn.");
        if (Constants.RANDOM.nextBoolean()) {
            Eventor entertainer = ApplicationState.getAnyFreeEventorByPurpose(Purpose.PET_ENTERTAINMENT);
            if (entertainer != null && !entertainer.equals(this)) {
                Logger.getInstance().log(Logger.Level.DETAIL, this + " will entertain.");
                new GettingEntertained(playfulness * periodInSecs, this, entertainer);
                return;
            } else {
                Logger.getInstance().log(Logger.Level.WARNING, this + " hasn't found any entertaining eventors.");
            }
        }

        Logger.getInstance().log(Logger.Level.INFO, this + " decides to wait.");
        new Waiting((Constants.RANDOM.nextInt(Constants.PERIODS_TO_WAIT_BOUND_OF_PET) + 1) * periodInSecs, this);
    }
}
