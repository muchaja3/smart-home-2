package actors.houseMembers;

import actors.Eventor;
import general.Constants;
import spaces.Room;

public abstract class HouseMember extends Eventor {

    public FamilyStatus familyStatus;
    public int age;
    public boolean isMaleNotFemale;
    public int maxEnergy;

    private int energy;
    /**
     * how much entertained {@code this} feels
     */
    private int amusement;

    public HouseMember(String name, Room room, FamilyStatus familyStatus, int age, boolean isMaleNotFemale, int energy, int maxEnergy, int amusement) {
        super(name, Constants.PURPOSES_OF_HOUSE_MEMBER, room);
        this.familyStatus = familyStatus;
        this.age = age;
        this.isMaleNotFemale = isMaleNotFemale;
        this.energy = energy;
        this.maxEnergy = maxEnergy;
        this.amusement = amusement;
    }

    @Override
    public void act(int periodInSecs) {
        changeAmusement(-periodInSecs);
        changeEnergy(-periodInSecs);
        if (isBusy()) {
            actToLive(periodInSecs);
        }
    }

    /**
     * Use the turn of this house member to keep {@code this}'s attributes on good levels.
     *
     * @param periodInSecs time since the last turn expressed in seconds
     */
    protected abstract void actToLive(int periodInSecs);

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    /**
     * Change energy the way it doesn't get lower than 0.
     *
     * @param change addend to the old {@code energy}. Can be negative.
     */
    public void changeEnergy(int change) {
        this.energy = Math.max(this.energy + change, 0);
    }

    public int getAmusement() {
        return amusement;
    }

    /**
     * Change amusement the way it doesn't get lower than 0.
     *
     * @param change addend to the old {@code amusement}. Can be negative.
     */
    public void changeAmusement(int change) {
        this.amusement = Math.max(this.amusement + change, 0);
    }


    @Override
    public String toString() {
        return familyStatus + "{name='" + getName() + "'}";
    }
}
