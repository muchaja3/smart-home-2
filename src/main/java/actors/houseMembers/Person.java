package actors.houseMembers;

import actors.devices.Device;
import actors.Entertainer;
import actors.Eventor;
import actors.Purpose;
import general.ApplicationState;
import general.Constants;
import general.Logger;
import events.*;
import spaces.Room;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.beans.ConstructorProperties;

@JsonTypeName("person")
public class Person extends HouseMember implements Entertainer {

    private final int SECS_TO_REPAIR;

    private final int PLAYFULNESS_IN_SECS;

    @ConstructorProperties({"name", "room", "familyStatus", "age", "isMaleNotFemale", "energy", "maxEnergy", "amusement", "secsToRepair", "funPerSec", "playfulnessInSecs"})
    public Person(String name, Room room, FamilyStatus familyStatus, int age, boolean isMaleNotFemale, int energy, int maxEnergy, int amusement, int secsToRepair, int funPerSec, int playfulnessInSecs) {
        super(name, room, familyStatus, age, isMaleNotFemale, energy, maxEnergy, amusement);
        this.funPerSec = funPerSec;
        this.SECS_TO_REPAIR = secsToRepair;
        this.PLAYFULNESS_IN_SECS = playfulnessInSecs;
        Logger.getInstance().log(Logger.Level.INFO, "created a person " + this);
    }

    @Override
    public void actToLive(int periodInSecs) {
        Logger.getInstance().log(Logger.Level.DETAIL, this + " is deciding what to do with his turn.");
        if (getEnergy() < maxEnergy * Constants.SLEEPY_ENERGY_BOUND_FRACTION_OF_PERSON) {
            Logger.getInstance().log(Logger.Level.DETAIL, this + " doesn't have much energy. (energy: " + getEnergy() + ". They decide to wait.");
            new Waiting(Constants.RANDOM.nextInt(maxEnergy - getEnergy()), this);

        } else if (getAmusement() < Constants.BORING_AMUSEMENT_BOUND_OF_PERSON) {
            if (getEnergy() > maxEnergy * Constants.VIGOROUS_ENERGY_BOUND_FRACTION_OF_PERSON) {
                Logger.getInstance().log(Logger.Level.DETAIL, this + " uses his high level of energy. (energy: " + getEnergy() + ".");
                enjoyWithHighEnergy();
            } else {
                Logger.getInstance().log(Logger.Level.DETAIL, this + " uses his middle level of energy. (energy: " + getEnergy() + ".");
                enjoyWithMiddleEnergy();
            }
        } else {
            Logger.getInstance().log(Logger.Level.DETAIL, this + " will do something usefull.");
            doSomethingUseful();
        }
    }

    /**
     * Use the turn assuming {@code this} has high level of energy.
     */
    private void enjoyWithHighEnergy() {
        boolean entertainWithoutSport = Constants.RANDOM.nextBoolean();
        if (entertainWithoutSport) {
            Eventor entertainer = ApplicationState.getAnyFreeEventorByPurpose(Purpose.HUMAN_ENTERTAINMENT);
            if (entertainer != null && entertainer != this) {
                Logger.getInstance().log(Logger.Level.DETAIL, this + "'s high energy will be used for entertainment.");
                new GettingEntertained(PLAYFULNESS_IN_SECS, this, entertainer);
                return;
            }
        }
        Eventor usedEventor = ApplicationState.getAnyFreeEventorByPurpose(Purpose.SPORT);
        if (usedEventor != null && usedEventor != this) {
            Logger.getInstance().log(Logger.Level.DETAIL, this + "'s high energy will be used for sporting.");
            new Sporting(PLAYFULNESS_IN_SECS, this, usedEventor);
        }
    }

    /**
     * Use the turn assuming {@code this} has about average level of energy.
     */
    private void enjoyWithMiddleEnergy() {
        Logger.getInstance().log(Logger.Level.DETAIL, this + "'s middle energy will be used for entertainment.");
        new GettingEntertained(PLAYFULNESS_IN_SECS, this, ApplicationState.getAnyFreeEventorByPurpose(Purpose.HUMAN_ENTERTAINMENT));
    }

    /**
     * Repair some broken device in the house or work if there is nothing to repair.
     */
    private void doSomethingUseful() {
        if (!ApplicationState.getBrokenDevices().isEmpty()) {
            Logger.getInstance().log(Logger.Level.DETAIL, this + " decides to look for and repair for something broken.");
            repairAnythingBroken();
        } else {
            Logger.getInstance().log(Logger.Level.DETAIL, this + " decides to work.");
            work();
        }
    }

    private void repairAnythingBroken() {
        Device repairedDevice = ApplicationState.getBrokenDevices().get(Constants.RANDOM.nextInt(ApplicationState.getBrokenDevices().size()));
        new Repair(SECS_TO_REPAIR, this, repairedDevice);
    }

    private void work() {
        int timeToWork = Constants.RANDOM.nextInt(this.getEnergy());
        Device chosenDevice = (Device) ApplicationState.getAnyFreeEventorByPurpose(Purpose.WORK);
        new Working(timeToWork, this, chosenDevice);
    }


    @Override
    public int getFunWithinDefinedTime(int timeInSecs) {
        //when they can't concentrate anymore, there is no fun in playing with them
        return Math.min(timeInSecs, PLAYFULNESS_IN_SECS) * getFunPerSec();
    }
}
