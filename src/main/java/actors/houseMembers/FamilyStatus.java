package actors.houseMembers;

public enum FamilyStatus {
    BABY, CHILD, PARENT, GRANDPARENT, DOG, CAT
}
