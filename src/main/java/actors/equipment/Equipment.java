package actors.equipment;

import actors.Eventor;
import general.Constants;
import spaces.Room;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.beans.ConstructorProperties;

@JsonTypeName("equipment")
public class Equipment extends Eventor {

    private String color;
    private EquipmentType equipmentType;

    @ConstructorProperties({"name", "room", "color", "equipmentType"})
    public Equipment(String name, Room room, String color, EquipmentType equipmentType) {
        super(name, Constants.PURPOSES_OF_EQUIPMENT, room);
        this.color = color;
        this.equipmentType = equipmentType;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

    @Override
    public void act(int periodInSecs) {
        //do nothing
    }

    @Override
    public String toString() {
        return "Equipment{" +
                "equipmentType=" + equipmentType +
                ", color='" + color + '\'' +
                '}';
    }
}
