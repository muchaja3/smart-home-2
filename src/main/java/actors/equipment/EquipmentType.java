package actors.equipment;

public enum EquipmentType {
    SKI, BICYCLE, CAR
}
