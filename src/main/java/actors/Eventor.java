package actors;

import actors.devices.CDPlayer;
import actors.devices.CustomDevice;
import actors.devices.PC;
import actors.devices.TV;
import actors.equipment.Equipment;
import actors.houseMembers.Person;
import actors.houseMembers.Pet;
import actors.iterating.Quantifyable;
import general.ApplicationState;
import spaces.Room;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.*;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Person.class, name = "person"),
        @JsonSubTypes.Type(value = Pet.class, name = "pet"),
        @JsonSubTypes.Type(value = PC.class, name = "pc"),
        @JsonSubTypes.Type(value = TV.class, name = "tv"),
        @JsonSubTypes.Type(value = CDPlayer.class, name = "cdPlayer"),
        @JsonSubTypes.Type(value = CustomDevice.class, name = "customDevice"),
        @JsonSubTypes.Type(value = Equipment.class, name = "equipment")
})

public abstract class Eventor implements Entertainer, Quantifyable<Eventor> {

    protected int funPerSec;

    private final String name;

    @JsonBackReference
    private Room room;

    private final Set<Purpose> PURPOSES;

    private int busyInSecs = 0;

    public Eventor(String name, Set<Purpose> PURPOSES, Room room) {
        this.name = name;
        this.PURPOSES = PURPOSES;
        this.room = room;

        ApplicationState.mapEventor(this);
    }

    public void becomeBusyForSecs(int timeInSecs) {
        busyInSecs += timeInSecs;
    }

    protected void decrementBusyInSecs() {
        busyInSecs--;
    }

    public abstract void act(int periodInSecs);

    @Override
    public boolean hasNoEventors() {
        return false;
    }

    @Override
    public List<Eventor> getQuantifiedCollection() {
        List<Eventor> abc = new ArrayList<>();
        abc.add(this);
        return abc;
    }

    @Override
    public final int getFunPerSec() {
        return funPerSec;
    }

    public int getBusyInSecs() {
        return busyInSecs;
    }

    public boolean isBusy() {
        return getBusyInSecs() == 0;
    }

    public String getName() {
        return name;
    }

    public Set<Purpose> getPurposes() {
        return PURPOSES;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
