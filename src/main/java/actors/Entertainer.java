package actors;

public interface Entertainer {

    /**
     * Calculates and returns the amount of fun by this entertainer gives to the entertained eventor for a given time.
     * @param timeInSecs Time spent by entertaining. Expressed in seconds.
     * @return the amount of amusement entertainer can offer for the specified time.
     */
    default int getFunWithinDefinedTime(int timeInSecs) {
        return getFunPerSec() * timeInSecs;
    }

    /**
     * Returns the amount of fun this entertainer gives to the entertained eventor per second.
     * @return raise in amusement for one second of entertaining
     */
    int getFunPerSec();
}
