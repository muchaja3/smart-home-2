package actors.devices;

import actors.devices.consumption.Consumption;
import actors.Entertainer;
import general.Constants;
import general.Logger;
import spaces.Room;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.beans.ConstructorProperties;

@JsonTypeName("tv")
public class TV extends Device implements Entertainer {

    private int channel;

    @ConstructorProperties({"name", "room"})
    protected TV(String name, Room room) {
        this(name, room, Constants.DEFAULT_CONSUMPTION_PER_SEC_OF_TV);
    }

    /**
     * {@code TV} constructor with the widest configurability.
     *
     * @param name              name of the TV
     * @param room              location of the TV
     * @param consumptionPerSec how much energy the {@code TV} will consume per second
     */
    protected TV(String name, Room room, int consumptionPerSec) {
        super(name, Constants.PURPOSES_OF_TV, room, new Consumption(Constants.SUPPLY_TYPE_OF_TV, consumptionPerSec));
        funPerSec = Constants.FUN_PER_SEC_OF_TV;
        Logger.getInstance().log(Logger.Level.INFO, "created a TV " + this);
    }


    public void incrementChannel() {
        channel++;
    }

    public void decrementChannel() {
        channel--;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    @Override
    protected double getProbabilityOfBreachPerSec() {
        return Constants.PROBABILITY_OF_BREACH_PER_SEC_OF_TV;
    }

    @Override
    public String toString() {
        return getName();
    }
}
