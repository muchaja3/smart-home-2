package actors.devices.consumption;

public enum SupplyType {
    ENERGY, GAS, WATER
}
