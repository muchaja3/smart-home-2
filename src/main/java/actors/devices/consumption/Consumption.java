package actors.devices.consumption;

public class Consumption {

    private final SupplyType supplyType;
    private int consumedAmount = 0;
    private final int amountPerSec;

    public Consumption(SupplyType supplyType, int amountPerSec) {
        this.supplyType = supplyType;
        this.amountPerSec = amountPerSec;
    }

    public void incrementBySecs(int timeInSecs) {
        consumedAmount += amountPerSec * timeInSecs;
    }

    public SupplyType getSupplyType() {
        return supplyType;
    }

    public int getConsumedAmount() {
        return consumedAmount;
    }

    @Override
    public String toString() {
        return "Consumption{" +
                "supplyType=" + supplyType +
                ", amountPerSec=" + amountPerSec +
                ", amountConsumed=" + consumedAmount +
                '}';
    }
}
