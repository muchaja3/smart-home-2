package actors.devices.powerState;

import actors.devices.Device;
import com.fasterxml.jackson.annotation.JsonBackReference;


public abstract class PowerState {

    @JsonBackReference
    protected Device device;

    /**
     * Sets the state of the {@code device} to a new {@code OffState} if it's not broken.
     */
    public abstract void switchOff();

    /**
     * Sets the state of the {@code device} to a new {@code OnState} if it's not broken.
     */
    public abstract void switchOn();

    /**
     * Sets the state of the {@code device} to a new {@code BrokenState}. If it's state is already one of that kind, it stays the same.
     */
    public abstract void doBreak();

    /**
     * Sets the state of the {@code device} to a new {@code OffState} - therefore it won't be broken any longer.
     */
    public abstract void repair();

    /**
     * Says if consumption should be incremented.
     * @return {@code true} if devices have positive consumption when in this state, {@code false} otherwise.
     */
    public abstract boolean doesConsume();
}
