package actors.devices.powerState;


public class OnState extends PowerState {
    @Override
    public void switchOff() {
        this.device.setPowerState(new OffState());
    }

    @Override
    public void switchOn() {
        //do nothing, the device is already switched on
    }

    @Override
    public void doBreak() {
        this.device.setPowerState(new BrokenState());
    }

    @Override
    public void repair() {
        //do nothing, the device is already OK
    }

    @Override
    public boolean doesConsume() {
        return true;
    }
}
