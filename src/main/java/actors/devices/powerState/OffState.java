package actors.devices.powerState;

public class OffState extends PowerState {
    @Override
    public void switchOff() {
        //do nothing, the device is already switched off
    }

    @Override
    public void switchOn() {
        this.device.setPowerState(new OnState());
    }

    @Override
    public void doBreak() {
        this.device.setPowerState(new BrokenState());
    }

    @Override
    public void repair() {
        //do nothing, the device is already OK
    }

    @Override
    public boolean doesConsume() {
        return false;
    }
}
