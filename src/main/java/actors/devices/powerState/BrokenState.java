package actors.devices.powerState;

public class BrokenState extends PowerState {

    @Override
    public void switchOff() {
        //do nothing, the device stays broken
    }

    @Override
    public void switchOn() {
        //do nothing, the device stays broken
    }

    @Override
    public void doBreak() {
        //do nothing, the device is already broken
    }

    @Override
    public void repair() {
        this.device.setPowerState(new OffState());
    }

    @Override
    public boolean doesConsume() {
        return false;
    }
}
