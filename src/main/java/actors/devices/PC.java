package actors.devices;

import actors.devices.consumption.Consumption;
import actors.Entertainer;
import general.Constants;
import general.Logger;
import spaces.Room;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.beans.ConstructorProperties;

@JsonTypeName("pc")
public class PC extends Device implements Entertainer {

    private String usedApplication = "";

    @ConstructorProperties({"name", "room"})
    public PC(String name, Room room) {
        this(name, room, Constants.DEFAULT_CONSUMPTION_PER_SEC_OF_PC);
    }

    /**
     * {@code PC} constructor with the widest configurability.
     * @param name name of the PC
     * @param room location of the PC
     * @param consumptionPerSec how much energy the {@code PC} will consume per second
     */
    public PC(String name, Room room, int consumptionPerSec) {
        super(name, Constants.PURPOSES_OF_PC, room, new Consumption(Constants.SUPPLY_TYPE_OF_PC, consumptionPerSec));
        funPerSec = Constants.FUN_PER_SEC_OF_PC;
        Logger.getInstance().log(Logger.Level.INFO, "created a PC " + this);
    }

    public String getUsedApplication() {
        return usedApplication;
    }

    public void setUsedApplication(String usedApplication) {
        this.usedApplication = usedApplication;
    }

    @Override
    protected double getProbabilityOfBreachPerSec() {
        return Constants.PROBABILITY_OF_BREACH_PER_SEC_OF_PC;
    }

    @Override
    public String toString() {
        return getName();
    }
}
