package actors.devices;

import actors.devices.consumption.Consumption;
import actors.devices.powerState.OnState;
import actors.devices.powerState.PowerState;
import actors.Eventor;
import actors.Purpose;
import general.ApplicationState;
import general.Constants;
import events.Breach;
import spaces.Room;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.Set;

public abstract class Device extends Eventor {

    private final Consumption consumption;
    @JsonManagedReference
    private PowerState powerState = new OnState();
    private final int age;

    /**
     * {@code Device} constructor. Tells the {@code Reporter} to note the new device down.
     *
     * @param name        name of the {@code Device}
     * @param purposes    what it can be used for
     * @param room        location of the {@code Device}
     * @param consumption how much energy the {@code Device} will consume per second
     */
    protected Device(String name, Set<Purpose> purposes, Room room, Consumption consumption) {
        super(name, purposes, room);
        this.consumption = consumption;
        this.age = Constants.DEFAULT_AGE_OF_DEVICES;
        ApplicationState.reporter.recordConsumer(this);
    }

    @Override
    public void act(int periodInSecs) {
        updateConsumption(periodInSecs);
        if (this.getBusyInSecs() != 0) {
            if (breaksDuringPeriod(periodInSecs)) {
                new Breach(this);
            }
        } else {
            this.decrementBusyInSecs();
        }
    }

    private boolean breaksDuringPeriod(int periodInSecs) {
        double rndNumber = Constants.RANDOM.nextFloat();
        double breachProbability = 1 - Math.pow(1 - this.getProbabilityOfBreachPerSec(), periodInSecs);
        return rndNumber < breachProbability;
    }

    /**
     * Returns how much it's probable that the device breaks during one second (assuming it's in a repaired state).
     * @return number representating the chance that this device breaks in one second. For example 0.01 = 1%.
     * Note that to correctly calculate the breach chance in another time period, the returned value can't be simply
     * multiplied.
     */
    protected abstract double getProbabilityOfBreachPerSec();

    /**
     * Use the state of this device, it's consumption and the argument {@code pastSecs} to calculate current total consumption
     *
     * @param passedSecs seconds passed since this method was called the last time
     */
    public void updateConsumption(int passedSecs) {
        if (this.powerState.doesConsume()) {
            consumption.incrementBySecs(passedSecs);
        }
    }

    /**
     * Set the state of this device to a new {@code OnState} if it's not broken.
     */
    public void switchOff() {
        this.powerState.switchOff();
    }

    /**
     * Set the state of this device to a new {@code OnState} if it's not broken.
     */
    public void switchOn() {
        this.powerState.switchOn();
    }

    /**
     * Set the state of this device to a new {@code BrokenState} and add this device to {@code brokenDevices}. Nothing happens if the device is already broken.
     */
    public void doBreak() {
        this.powerState.doBreak();
        ApplicationState.brokenDevices.add(this);
    }

    /**
     * Set the state of this device to a new {@code OffState} and removes this device from {@code brokenDevices}.
     */
    public void repair() {
        this.powerState.repair();
        ApplicationState.brokenDevices.remove(this);
    }

    public Consumption getConsumption() {
        return consumption;
    }

    public PowerState getPowerState() {
        return powerState;
    }

    public void setPowerState(PowerState powerState) {
        this.powerState = powerState;
    }

    public int getAge() {
        return age;
    }
}
