package actors.devices;

import actors.devices.consumption.Consumption;
import actors.Entertainer;
import general.Constants;
import general.Logger;
import spaces.Room;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.beans.ConstructorProperties;

@JsonTypeName("cdPlayer")
public class CDPlayer extends Device implements Entertainer {

    private String contains = "";


    @ConstructorProperties({"name", "room"})
    protected CDPlayer(String name, Room room) {
        this(name, room, Constants.DEFAULT_CONSUMPTION_PER_SEC_OF_CD_PLAYER);
    }

    /**
     * {@code CDPlayer} constructor with the widest configurability.
     *
     * @param name              name of the CDPlayer
     * @param room              location of the CDPlayer
     * @param consumptionPerSec how much energy the {@code CDPlayer} will consume per second
     */
    protected CDPlayer(String name, Room room, int consumptionPerSec) {
        super(name, Constants.PURPOSES_OF_CD_PLAYER, room, new Consumption(Constants.SUPPLY_TYPE_OF_CD_PLAYER, consumptionPerSec));
        funPerSec = Constants.FUN_PER_SEC_OF_CD_PLAYER;
        Logger.getInstance().log(Logger.Level.INFO, "created a CD player " + this);
    }


    public String getContains() {
        return contains;
    }

    public void setContains(String contains) {
        this.contains = contains;
    }

    @Override
    protected double getProbabilityOfBreachPerSec() {
        return Constants.PROBABILITY_OF_BREACH_PER_SEC_OF_CD_PLAYER;
    }

    @Override
    public String toString() {
        return getName();
    }
}
