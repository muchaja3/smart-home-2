package actors.devices;

import actors.devices.consumption.Consumption;
import actors.devices.consumption.SupplyType;
import actors.Purpose;
import general.Constants;
import general.Logger;
import spaces.Room;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.beans.ConstructorProperties;
import java.util.Set;

@JsonTypeName("customDevice")
public class CustomDevice extends Device {

    private final double PROBABILITY_OF_BREACH_PER_SEC;

    @ConstructorProperties({"name", "purposes", "room", "supplyType", "consumptionPerSec"})
    public CustomDevice(String name, Set<Purpose> purposes, Room room, SupplyType supplyType, int consumptionPerSec) {
        super(name, purposes, room, new Consumption(supplyType, consumptionPerSec));
        this.PROBABILITY_OF_BREACH_PER_SEC = Constants.DEFAULT_PROBABILITY_OF_BREACH_PER_SEC_OF_CUSTOM_DEVICE;
        Logger.getInstance().log(Logger.Level.INFO, "created a Custom Device " + this);
    }

    public CustomDevice(String name, Set<Purpose> purposes, Room room, SupplyType supplyType, int consumptionPerSec, double probabilityOfBreachPerSec) {
        super(name, purposes, room, new Consumption(supplyType, consumptionPerSec));
        this.PROBABILITY_OF_BREACH_PER_SEC = probabilityOfBreachPerSec;
        Logger.getInstance().log(Logger.Level.INFO, "created a Custom Device " + this);
    }


    @Override
    protected double getProbabilityOfBreachPerSec() {
        return PROBABILITY_OF_BREACH_PER_SEC;
    }

    @Override
    public String toString() {
        return getName();
    }
}
