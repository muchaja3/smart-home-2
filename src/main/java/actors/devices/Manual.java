package actors.devices;

public class Manual {

    private final String TEXT =
            "You have no rights to reclaim your money for this device.\n" +
                    "Order a new ";
    private final String TEXT_2 = " today via e-shop or visit one of our brick-and-mortar shops.\n" +
            "If you want to repair this device, smash it into a wall and stomp the devil out of it." +
            "Then it's about 50/50. No guarantee. No, we don't think we are funny.";

    /**
     * Class providing lazily loaded environment.
     */
    private static class ManualTemplateHolder {
        private static final Manual TEMPLATE = new Manual();
    }

    /**
     * Lazily loads the (singleton) instance.
     * @return the loaded instance.
     */
    private static Manual getTemplate() {
        return ManualTemplateHolder.TEMPLATE;
    }

    /**
     * Uses lazily loaded schedule and the device's name to make up a string serving as the manual's content.
     * @param targetDevice which manual text should be returned.
     * @return text in manual made for this device.
     */
    public static String buildManualText(Device targetDevice) {
        return getTemplate().TEXT + targetDevice.getName() + getTemplate().TEXT_2;
    }
}
