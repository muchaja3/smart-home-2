package actors.iterating;

import actors.Eventor;
import spaces.House;

import java.util.Iterator;

public class EventorsCollection implements Iterable<Eventor> {

    private final House house;

    public EventorsCollection(House house) {
        this.house = house;
    }

    @Override
    public Iterator<Eventor> iterator() {
        return new EventorIterator(house);
    }
}
