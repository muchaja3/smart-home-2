package actors.iterating;

import java.util.List;

public interface Quantifyable<T> {

    /**
     * Tells if this {@code Quantifyable} contains any {@code Eventor}.
     * @return {@code true} if there is no {@code Eventor} contained in this {@code Quantifyable}, {@code} false otherwise
     */
    default boolean hasNoEventors() {
        for (T space : getQuantifiedCollection()) {
            if (!((Quantifyable<?>) space).hasNoEventors()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Getter of the collection that is supposed to be quantified (contains eventors that should be counted).
     * @return The collection that is supposed to be quantified.
     */
    List<T> getQuantifiedCollection();
}
