package actors.iterating;

import actors.Eventor;
import exceptions.InvalidConfigException;
import spaces.House;

import java.util.Iterator;

public class EventorIterator implements Iterator<Eventor> {

    private Eventor currentEventor;
    private final House house;

    private final int[] indexesERF = {0, 0, 0};

    EventorIterator(House house) {
        this.house = house;
        if (house == null || house.hasNoEventors()) {
            return;
        }

        try {
            this.currentEventor =
                    house.getFloors().get(0)
                            .getRooms().get(0)
                            .getEventors().get(0);
        }
        catch (IndexOutOfBoundsException e) {
            System.exit(1);
        }
    }

    @Override
    public boolean hasNext() {
        return currentEventor != null;
    }

    @Override
    public Eventor next() {
        Eventor returnedValue = currentEventor;
        currentEventor = findNext();
        return returnedValue;
    }

    private Eventor findNext() {
        for (int level = 0; level < indexesERF.length; level++) {
            indexesERF[level]++;
            if (!overflowsBelowLevel(level)) {
                return (Eventor) getSpaceByIndexes(0);
            }
        }
        return null;
    }

    private boolean overflowsBelowLevel(int level) {
        for (int checkedLevel = level; checkedLevel >= 0; checkedLevel--) {
            if (overflowsOnLevel(checkedLevel)) {
                return true;
            }
        }
        return false;
    }

    private boolean overflowsOnLevel(int level) {
        Quantifyable space = (Quantifyable) getSpaceByIndexes(level + 1);

        if (indexesERF[level] >= space.getQuantifiedCollection().size()) {
            indexesERF[level] = 0;
            return true;
        } else {
            return false;
        }
    }

    private Object getSpaceByIndexes(int requiredLevel) {
        Object space = house;
        for (int spaceLevel = indexesERF.length - 1; spaceLevel >= requiredLevel; spaceLevel--) {
            space = ((Quantifyable) space).getQuantifiedCollection().get(indexesERF[spaceLevel]);
        }
        return space;
    }
}
