package general;

import actors.devices.consumption.Consumption;
import actors.devices.consumption.SupplyType;
import actors.devices.Device;
import actors.Eventor;
import events.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Reporter {

    private String houseConfigReport;
    private List<Event> recordedEvents = new ArrayList<>();
    private final Set<Device> consumers = new HashSet<>();

    private Reporter() {
    }

    private static class ReporterHolder {
        private static final Reporter REPORTER = new Reporter();
    }

    public static Reporter getInstance(String houseConfigReport) {
        ReporterHolder.REPORTER.setHouseConfigReport(houseConfigReport);
        return getInstance();
    }

    public static Reporter getInstance() {
        return ReporterHolder.REPORTER;
    }

    /**
     * Prints all the assigned reports. Remember to call {@code setHouseConfigReport(String)} method and {@code recordConsumer(Device)} the right way before invoking this one.
     */
    public void printRecords() {
        this.recordedEvents = sortRecorded(this.recordedEvents);

        printHouseConfigReport();
        printEventReport();
        printActivityAndUsageReport();
        printConsumptionReport();
    }

    /**
     * Sorts events by class, names of it's participants and returns new list with sorted content.
     *
     * @return new list with sorted recorded {@code Event}s.
     */
    public List<Event> sortRecorded(List<? extends Event> recorded) {
        Stream<? extends Event> sortedRecorded = recorded.stream().sorted(Comparator.comparing(a -> a.getClass().getName()));

        Integer maxNumberOfParticipants = recorded.stream().map(e -> e.getParticipants().size()).max(Comparator.naturalOrder()).orElse(0);
        for (int participantI = maxNumberOfParticipants; participantI >= 0; participantI--) {
            final int finalParticipantI = participantI;
            sortedRecorded = sortedRecorded.sorted(Comparator.comparing(event -> event.getParticipants().size() > finalParticipantI ?
                    event.getParticipants().get(finalParticipantI).getName() : ""));
        }

        return sortedRecorded.collect(Collectors.toCollection(ArrayList::new));
    }


    private void printHouseConfigReport() {
        Logger.getInstance().logReport("--- House-Config Report ---");
        Logger.getInstance().logReport(this.houseConfigReport);
    }


    private void printEventReport() {
        Logger.getInstance().logReport("--- Event Report ---");
        for (Event event : this.recordedEvents) {
            Logger.getInstance().logReport(event.toString());
        }
    }


    private void printActivityAndUsageReport() {
        Logger.getInstance().logReport("--- Activity-and-Usage Report ---");
        printGettingsEntertainedActivityReport();
        printRepairsActivityReport();
        printSportingActivityReport();
        printWorkingActivityReport();
    }

    private void printGettingsEntertainedActivityReport() {
        List<GettingEntertained> gettingsEntertained = recordedEvents.stream()
                .filter(ge -> ge.getClass() == GettingEntertained.class)
                .map(ge -> (GettingEntertained) ge).collect(Collectors.toCollection(ArrayList::new));
        Map<Eventor, Map<Eventor, Integer>> reportingTree = fillGettingEntertainedReportingTree(gettingsEntertained);

        printTree(reportingTree, " entertained themselves with ");
    }

    private Map<Eventor, Map<Eventor, Integer>> fillGettingEntertainedReportingTree(List<GettingEntertained> gettingsEntertained) {
        Map<Eventor, Map<Eventor, Integer>> actionReportingTree = new HashMap<>();

        for (GettingEntertained gettingEntertained : gettingsEntertained) {

            Eventor originator = gettingEntertained.getOriginator();
            Eventor entertainer = gettingEntertained.getEntertainer();

            incrementCount(actionReportingTree, originator, entertainer);
        }

        return actionReportingTree;
    }

    private void printRepairsActivityReport() {
        List<Repair> repairs = recordedEvents.stream()
                .filter(ge -> ge.getClass() == Repair.class)
                .map(ge -> (Repair) ge).collect(Collectors.toCollection(ArrayList::new));
        Map<Eventor, Map<Eventor, Integer>> reportingTree = fillRepairReportingTree(repairs);

        printTree(reportingTree, " repaired ");
    }

    private Map<Eventor, Map<Eventor, Integer>> fillRepairReportingTree(List<Repair> repairs) {
        Map<Eventor, Map<Eventor, Integer>> actionReportingTree = new HashMap<>();

        for (Repair gettingEntertained : repairs) {

            Eventor originator = gettingEntertained.getOriginator();
            Eventor repairedTarget = gettingEntertained.getRepairedTarget();

            incrementCount(actionReportingTree, originator, repairedTarget);
        }

        return actionReportingTree;
    }

    private void printSportingActivityReport() {
        List<Sporting> sportings = recordedEvents.stream()
                .filter(ge -> ge.getClass() == Sporting.class)
                .map(ge -> (Sporting) ge).collect(Collectors.toCollection(ArrayList::new));
        Map<Eventor, Map<Eventor, Integer>> reportingTree = fillSportingReportingTree(sportings);

        printTree(reportingTree, " sported with ");
    }

    private Map<Eventor, Map<Eventor, Integer>> fillSportingReportingTree(List<Sporting> sportings) {
        Map<Eventor, Map<Eventor, Integer>> actionReportingTree = new HashMap<>();

        for (Sporting gettingEntertained : sportings) {

            Eventor originator = gettingEntertained.getOriginator();
            Eventor tool = gettingEntertained.getTool();

            incrementCount(actionReportingTree, originator, tool);
        }

        return actionReportingTree;
    }

    private void printWorkingActivityReport() {
        List<Working> workings = recordedEvents.stream()
                .filter(ge -> ge.getClass() == Working.class)
                .map(ge -> (Working) ge).collect(Collectors.toCollection(ArrayList::new));
        Map<Eventor, Map<Eventor, Integer>> reportingTree = fillWorkingReportingTree(workings);

        printTree(reportingTree, " worked using ");
    }

    private Map<Eventor, Map<Eventor, Integer>> fillWorkingReportingTree(List<Working> workings) {
        Map<Eventor, Map<Eventor, Integer>> actionReportingTree = new HashMap<>();

        for (Working gettingEntertained : workings) {

            Eventor originator = gettingEntertained.getOriginator();
            Eventor tool = gettingEntertained.getUsedDevice();

            incrementCount(actionReportingTree, originator, tool);
        }

        return actionReportingTree;
    }

    private void incrementCount(Map<Eventor, Map<Eventor, Integer>> actionReportingTree, Eventor originator, Eventor tool) {
        actionReportingTree.putIfAbsent(originator, new HashMap<>());
        Map<Eventor, Integer> originatorsActionCounts = actionReportingTree.get(originator);
        originatorsActionCounts.putIfAbsent(tool, 0);
        originatorsActionCounts.put(tool, originatorsActionCounts.get(tool) + 1);
    }

    private void printTree(Map<Eventor, Map<Eventor, Integer>> reportingTree, String whatTheyDid) {
        for (Eventor originator : reportingTree.keySet()) {
            Map<Eventor, Integer> originatorsActionCounts = reportingTree.get(originator);
            for (Eventor entertainer : originatorsActionCounts.keySet()) {
                Integer count = originatorsActionCounts.get(entertainer);
                Logger.getInstance().logReport(originator.getName() + whatTheyDid + entertainer.getName() + " " + count + " times.");
            }
        }
    }


    private void printConsumptionReport() {
        Logger.getInstance().logReport("--- Consumption Report ---");
        for (Device consumer : this.consumers) {
            String name = consumer.getName();
            Consumption consumption = consumer.getConsumption();
            printConsumptionReportPiece(name, consumption.getConsumedAmount(), consumption.getSupplyType());
        }
    }

    private void printConsumptionReportPiece(String name, int consumedAmount, SupplyType supplyType) {
        String nameWithFirstUppercase;
        if (name.length() > 0) {
            nameWithFirstUppercase = Character.toUpperCase(name.charAt(0)) + name.substring(1);
        } else {
            nameWithFirstUppercase = "";
        }
        Logger.getInstance().logReport(nameWithFirstUppercase + " consumed " + consumedAmount + " of " + supplyType + ".");
    }


    public void setHouseConfigReport(String houseConfigReport) {
        this.houseConfigReport = houseConfigReport;
    }

    /**
     * Note down that the passed device's consumption should be reported when {@code printRecords()} is called.
     *
     * @param device Device which consumption is supposed to be reported later.
     */
    public void recordConsumer(Device device) {
        this.consumers.add(device);
    }

    /**
     * Notes down that the passed event should be reported when {@code printRecords()} is called.
     * In other words, adds {@code this} to the field containing recorded events of the subclass.
     *
     * @param event Event which is supposed to be reported later.
     */
    public void recordEvent(Event event) {
        this.recordedEvents.add(event);
    }
}
