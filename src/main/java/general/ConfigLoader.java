package general;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import exceptions.InvalidConfigException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ConfigLoader {

    private final String CONFIG_FILE = "src/main/resources/config.json";

    /**
     * Read and make the instance of {@code ConfigData} containing data from the JSON configuration file.
     *
     * @return The instance of {@code ConfigData} containing data from the JSON configuration file.
     */
    public ConfigData loadConfig() throws InvalidConfigException {

        ConfigData data = null;

        String fileType = CONFIG_FILE.substring((CONFIG_FILE.length() - 4));
        if (!(fileType.equals("json"))) {
            throw new InvalidConfigException("Config is not a json file!");
        }

        try {
            ObjectMapper mapper = new ObjectMapper();

            data = mapper.readValue(Paths.get(CONFIG_FILE).toFile(), ConfigData.class);

            Logger.getInstance().log(Logger.Level.INFO, data.getHouse().getFamilyName() + " " + data.getHouse().getFloors());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    /**
     * Checks whether each room contains at least one eventor in JSON file.
     * Throws InvalidConfigException when room doesn't contain any eventors.
     */
    public void checkConfigData() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readValue(Paths.get(CONFIG_FILE).toFile(), JsonNode.class);
            JsonNode house = node.get("house");
            JsonNode floors = house.get("floors");

            for (int i = 0; i < floors.size(); i++) {
                JsonNode rooms = floors.get(i).get("rooms");
                for (int j = 0; j < rooms.size(); j++) {
                    JsonNode eventors = rooms.get(j).get("eventors");
                    if (eventors.size() < 1) {
                        throw new InvalidConfigException("Rooms should contain at least one eventor.");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readFileAsString() {
        try {
            return new String(Files.readAllBytes(Paths.get(CONFIG_FILE)));
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
