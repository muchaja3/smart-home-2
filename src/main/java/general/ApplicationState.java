package general;

import actors.Eventor;
import actors.Purpose;
import actors.devices.Device;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ApplicationState {
    public static Reporter reporter;
    public static final Map<Purpose, List<Eventor>> eventorsByPurpose = new HashMap<>();
    public static final List<Device> brokenDevices = new ArrayList<>();
    static {
        for (Purpose purpose : Purpose.values()) {
            eventorsByPurpose.put(purpose, new ArrayList<>());
        }
    }

    public static void setReporter(Reporter reporter) {
        ApplicationState.reporter = reporter;
    }

    /**
     * Returns a free random eventor (= not a busy one - where busy time equals 0) in this configuration run with the {@code Purpose} in argument.
     * @param desiredPurpose {@code Purpose} the returned eventor is supposed to have.
     * @return An {@code Eventor} instance with the desired {@code Purpose} and {@code busyInSecs} field higher than 0.
     */
    public static Eventor getAnyFreeEventorByPurpose(Purpose desiredPurpose) {
        List<Eventor> eventors = ApplicationState.eventorsByPurpose.get(desiredPurpose).stream()
                .filter(eventor -> eventor.getBusyInSecs() == 0)
                .collect(Collectors.toCollection(ArrayList::new));
        if (eventors.size() == 0) {
            return null;
        } else {
            return eventors.get(Constants.RANDOM.nextInt(eventors.size()));
        }
    }

    /**
     * Notes down the {@code Eventor} from the argument to return it later in methods getAnyFreeEventorByPurpose and getEventorsByPurpose.
     * @param newEventor eventor supposed to be noted down
     */
    public static void mapEventor(Eventor newEventor) {
        for (Purpose purposeOfNewEventor : newEventor.getPurposes()) {
            ApplicationState.eventorsByPurpose.get(purposeOfNewEventor).add(newEventor);
        }
    }

    public static List<Device> getBrokenDevices() {
        return brokenDevices;
    }
}
