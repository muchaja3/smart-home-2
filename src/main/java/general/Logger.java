package general;

import java.io.PrintStream;
import java.util.Arrays;

import static general.Logger.Level.INFO;

public class Logger {

    private final PrintStream PRINT_STREAM = System.out;
    private final Level DEFAULT_LOGGING_LEVEL = INFO;
    private final String MESSAGE_SEPARATOR = "; ";
    private int lowestLoggedLevel;

    private Logger(int lowestLoggedLevel) {
        this.lowestLoggedLevel = lowestLoggedLevel;
    }

    private static class LoggerInstanceHolder {
        private static final Logger LOGGER = new Logger(Constants.INITIAL_LOWEST_LOG_LEVEL_OF_LOGGER);
    }

    public enum Level {
        DETAIL("DETAIL: "), INFO("INFO: "), WARNING("WARNING! "), ERROR("ERROR!!    "), RESULT("");

        Level(String intro) {
            this.intro = intro;
        }

        final String intro;
    }

    /**
     * Automatically chooses the level of the report message and logs it.
     * Mind that even messages of the report level can be skipped depending on the {@code LOWEST_LOGGED_LEVEL} field.
     *
     * @param message text of the message.
     */
    public void logReport(String message) {
        log(Level.RESULT, message);
    }

    /**
     * Uses the default level of logging to log separately every message passed in the argument(s).
     * Mind that messages of some levels can be skipped depending on the {@code LOWEST_LOGGED_LEVEL} field.
     *
     * @param messages messages supposed to be logged.
     */
    public void log(String... messages) {
        log(DEFAULT_LOGGING_LEVEL, messages);
    }

    /**
     * Uses the defined level of logging to log separately every message passed in the argument(s).
     * Mind that messages of some levels can be skipped depending on the {@code LOWEST_LOGGED_LEVEL} field.
     *
     * @param messages messages supposed to be logged.
     */
    public void log(Level level, String... messages) {
        if (level.ordinal() < lowestLoggedLevel) return;

        StringBuilder stringBuilder = new StringBuilder();
        Arrays.stream(messages).forEach(message -> stringBuilder.append(message).append(MESSAGE_SEPARATOR));
        stringBuilder.setLength(stringBuilder.length() - MESSAGE_SEPARATOR.length());

        PRINT_STREAM.println(level.intro + stringBuilder);
    }

    public static Logger getInstance() {
        return LoggerInstanceHolder.LOGGER;
    }

    public void setLowestLoggedLevel(int lowestLoggedLevel) {
        this.lowestLoggedLevel = lowestLoggedLevel;
    }
}
