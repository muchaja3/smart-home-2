package general;

import actors.Purpose;
import actors.devices.consumption.SupplyType;

import java.util.Random;
import java.util.Set;

public class Constants {

    public static final Random RANDOM = new Random();

    public static final int INITIAL_LOWEST_LOG_LEVEL_OF_LOGGER = 0;


    public static final Set<Purpose> PURPOSES_OF_EQUIPMENT = Set.of(Purpose.SPORT, Purpose.HUMAN_ENTERTAINMENT);


    public static final int DEFAULT_AGE_OF_DEVICES = 0;

    public static final int FUN_PER_SEC_OF_CD_PLAYER = 1;
    public static final Set<Purpose> PURPOSES_OF_CD_PLAYER = Set.of(Purpose.HUMAN_ENTERTAINMENT);
    public static final SupplyType SUPPLY_TYPE_OF_CD_PLAYER = SupplyType.ENERGY;
    public static final int DEFAULT_CONSUMPTION_PER_SEC_OF_CD_PLAYER = 5;
    public static final double PROBABILITY_OF_BREACH_PER_SEC_OF_CD_PLAYER = 0.00000005;

    public static final int FUN_PER_SEC_OF_PC = 3;
    public static final Set<Purpose> PURPOSES_OF_PC = Set.of(Purpose.HUMAN_ENTERTAINMENT, Purpose.WORK);
    public static final SupplyType SUPPLY_TYPE_OF_PC = SupplyType.ENERGY;
    public static final int DEFAULT_CONSUMPTION_PER_SEC_OF_PC = 100;
    public static final double PROBABILITY_OF_BREACH_PER_SEC_OF_PC = 0.0001;

    public static final int FUN_PER_SEC_OF_TV = 2;
    public static final Set<Purpose> PURPOSES_OF_TV = Set.of(Purpose.HUMAN_ENTERTAINMENT);
    public static final SupplyType SUPPLY_TYPE_OF_TV = SupplyType.ENERGY;
    public static final int DEFAULT_CONSUMPTION_PER_SEC_OF_TV = 110;
    public static final double PROBABILITY_OF_BREACH_PER_SEC_OF_TV = 0.0000001;

    public static final double DEFAULT_PROBABILITY_OF_BREACH_PER_SEC_OF_CUSTOM_DEVICE = 0.0000001;


    public static final Set<Purpose> PURPOSES_OF_HOUSE_MEMBER = Set.of(Purpose.HUMAN_ENTERTAINMENT, Purpose.PET_ENTERTAINMENT);

    public static final int PERIODS_TO_WAIT_BOUND_OF_PET = 5;

    public static final double SLEEPY_ENERGY_BOUND_FRACTION_OF_PERSON = 1.0 / 3;
    public static final int BORING_AMUSEMENT_BOUND_OF_PERSON = 300;
    public static final double VIGOROUS_ENERGY_BOUND_FRACTION_OF_PERSON = 2.0 / 2;
}
