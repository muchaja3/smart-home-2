package general;

import spaces.House;

import java.beans.ConstructorProperties;

public class ConfigData {       //can be defined as a record instead of a class

    private final int periodInSecs;
    private final int numberOfIterations;
    private final House house;


    @ConstructorProperties({"periodInSecs", "numberOfIterations", "house"})
    public ConfigData(int periodInSecs, int numberOfIterations, House house) {
        this.periodInSecs = periodInSecs;
        this.numberOfIterations = numberOfIterations;
        this.house = house;
        Logger.getInstance().log(Logger.Level.INFO, "Configuration data created.");
    }

    public int getPeriodInSecs() {
        return periodInSecs;
    }

    public int getNumberOfIterations() {
        return numberOfIterations;
    }

    public House getHouse() {
        return house;
    }
}
