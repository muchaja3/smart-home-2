import actors.Eventor;
import general.*;
import exceptions.InvalidConfigException;

public class SimulationRunner {

    /**
     * Loads configuration file, simulates operations of the household, uses individual devices of the house
     * and evaluates the use, consumption, free and working time of individual people.
     * @param args unnecessary args which don't change the behaviour of this application at all.
     */
    public static void main(String[] args) throws InvalidConfigException {
        ConfigLoader configLoader = new ConfigLoader();
        Reporter reporter = Reporter.getInstance(configLoader.readFileAsString());      //getConfigReport should just return text of the loaded file

        ApplicationState.setReporter(reporter);

        ConfigData data = configLoader.loadConfig();
        configLoader.checkConfigData();

        for (int iteration = 0; iteration < data.getNumberOfIterations(); iteration++) {
            update(data);
        }

        reporter.printRecords();
    }

    /**
     * Runs one round. Gives every eventor a chance to act as many times as stated in loaded from the used configuration file.
     * @param data object containing data essential for the application to be run.
     */
    private static void update(ConfigData data) {
        for (Eventor eventor : data.getHouse().getEventorsCollection()) {
            eventor.act(data.getPeriodInSecs());
        }
    }
}
